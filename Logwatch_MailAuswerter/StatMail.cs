﻿using Microsoft.Office.Interop.Outlook;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logwatch_MailAuswerter
{
    class StatMail
    {

        //Variablen Deklaration
        string Host = "";
        string dateInitiated = "";
        string DPKG = "";
        string HTTPD = "";
        string postfix = "";
        string SSHD = "";
        string pam_unix = "";
        string clam = "";
        string fail2ban = "";
        string spamAssassin = "";
        string diskSpace = "";
        string dateRange = "";


        //Absender Adresse aufspalten und verkürzten Absender an nächste Methode übergeben
        public StatMail(MailItem item)
        {
            this.Host = item.Sender.Address.Split('@')[1];

            this.ParseMailItem(item);
        }

        private void ParseMailItem(MailItem item)
        {
            //Gesamte E-Mail in Liste umwandeln. --EINE LINE = 1 OBJEKT IN DER LISTE--
            List<string> lines = item.Body.Split('\n').ToList().Select(x => x.Trim()).ToList();
            int i = 0;


            //Line für Line durch die E-Mail laufen
            while (i < lines.Count)
            {
                string line = lines[i];


                //Wenn line leer --> Überspringen
                if (line == string.Empty)
                {

                }

                //Jede Line auf Übereinstimmungen mit relevanten Daten überprüfen. 


                else if (line.StartsWith("Processing Initiated"))
                {
                    dateInitiated = line;
                }
                else if (line.StartsWith("Date Range Processed"))
                {
                    string[] parts = lines[i + 1].Split('/');
                    if (parts.Length == 1)
                    {
                        //ein datum
                        dateRange = parts[0].ToString();
                    }
                    else
                    {
                        //start und enddatum
                        dateRange = parts[0].ToString() + " - " + parts[1].ToString();
                    }
                }
                else if (line.StartsWith("--------------------- dpkg status changes Begin ------------------------"))
                {
                    i = this.ReadDPKG(lines, i);
                }

                else if (line.StartsWith("--------------------- httpd Begin ------------------------"))
                {
                    i = this.ReadHTTPD(lines, i);
                }

                else if (line.StartsWith("--------------------- Postfix Begin ------------------------"))
                {
                    i = this.ReadPostfix(lines, i);
                }

                else if (line.StartsWith("--------------------- SSHD Begin ------------------------"))
                {
                    i = this.ReadSSHD(lines, i);
                }

                else if (line.StartsWith("--------------------- pam_unix Begin ------------------------"))
                {
                    i = this.ReadPam_Unix(lines, i);
                }

                else if (line.Contains("Date Range Processed:"))
                {
                    i = this.ReadDateRange(lines, i);
                }

                else if (line.StartsWith("--------------------- clam-update Begin ------------------------"))
                {
                    i = this.ReadClam(lines, i);
                }

                else if (line.StartsWith("--------------------- fail2ban-messages Begin ------------------------"))
                {
                    i = this.ReadFail2Ban(lines, i);
                }

                else if (line.StartsWith("--------------------- SpamAssassin Begin ------------------------"))
                {
                    i = this.ReadSpamAssassin(lines, i);
                }

                else if (line.StartsWith("--------------------- Disk Space Begin ------------------------"))
                {
                    i = this.ReadDiskSpace(lines, i);
                }

                else
                {
                    //Nichts
                }

                i++;
            }
        }

        //Wenn der Anfang eines relevanten Abschnitts gefunden wurde, den gesamten Abschnitt in eine Variable laden, 
        //zwischenspeichern und nach Abschluss des Abschnitts in der nächsten Zeile weiter lesen bis zum nächsten relevanten Abschnitt

        private int ReadDPKG(List<string> body, int i)
        {
            string endLine = "---------------------- dpkg status changes End -------------------------";

            while (i < body.Count && body[i] != endLine)
            {
                string line = body[i];
                DPKG += "\r\n";
                DPKG += body[i];

                i++;
            }

            return i;
        }

        private int ReadHTTPD(List<string> body, int i)
        {
            string endLine = "---------------------- httpd End -------------------------";

            while (i < body.Count && body[i] != endLine)
            {
                string line = body[i];
                HTTPD += "\r\n";
                HTTPD += body[i];

                i++;
            }

            return i;
        }

        private int ReadPostfix(List<string> body, int i)
        {
            string endLine = "---------------------- Postfix End -------------------------";

            while (i < body.Count && body[i] != endLine)
            {
                string line = body[i];
                postfix += "\r\n";
                postfix += body[i];

                i++;
            }

            return i;
        }

        private int ReadSSHD(List<string> body, int i)
        {
            string endLine = "---------------------- SSHD End -------------------------";

            while (i < body.Count && body[i] != endLine)
            {
                string line = body[i];
                SSHD += "\r\n";
                SSHD += body[i];

                i++;
            }

            return i;
        }

        private int ReadPam_Unix(List<string> body, int i)
        {
            string endLine = "---------------------- pam_unix End -------------------------";

            while (i < body.Count && body[i] != endLine)
            {
                string line = body[i];
                pam_unix += "\r\n";
                pam_unix += body[i];

                i++;
            }

            return i;
        }


        private int ReadDateRange(List<string> body, int i)
        {
            string endLine = "Period is day.";

            while (i < body.Count && body[i] != endLine)
            {

                string line = body[i];
                dateRange += "\r\n";
                dateRange += body[i];

                i++;
            }

            return i;
        }

        private int ReadClam(List<string> body, int i)
        {
            string endLine = "---------------------- clam-update End -------------------------";

            while (i < body.Count && body[i] != endLine)
            {
                string line = body[i];
                clam += "\r\n";
                clam += body[i];

                i++;
            }

            return i;
        }

        private int ReadFail2Ban(List<string> body, int i)
        {
            string endLine = "---------------------- fail2ban-messages End -------------------------";

            while (i < body.Count && body[i] != endLine)
            {
                string line = body[i];
                fail2ban += "\r\n";
                fail2ban += body[i];

                i++;
            }

            return i;
        }

        private int ReadSpamAssassin(List<string> body, int i)
        {
            string endLine = "---------------------- SpamAssassin End -------------------------";

            while (i < body.Count && body[i] != endLine)
            {
                string line = body[i];
                spamAssassin += "\r\n";
                spamAssassin += body[i];

                i++;
            }

            return i;
        }

        private int ReadDiskSpace(List<string> body, int i)
        {
            string endLine = "---------------------- Disk Space End -------------------------";

            while (i < body.Count && body[i] != endLine)
            {
                string line = body[i];
                diskSpace += "\r\n";
                diskSpace += body[i];

                i++;
            }

            return i;
        }


        //Methoden zum übergeben an Form1 
        public string GetDPKG()
        {
            return DPKG;
        }
        public string GetHTTPD()
        {
            return HTTPD;
        }
        public string GetPostfix()
        {
            return postfix;
        }
        public string GetSSHD()
        {
            return SSHD;
        }
        public string GetPam_Unix()
        {
            return pam_unix;
        }
        public string GetClam()
        {
            return clam;
        }
        public string GetFail2Ban()
        {
            return fail2ban;
        }
        public string GetSpamAssassin()
        {
            return spamAssassin;
        }
        public string GetDiskSpace()
        {
            return diskSpace;
        }
        public string GetDateRange()
        {
            return dateRange;
        }
        public string GetDateInitiated()
        {
            return dateInitiated;
        }
        public string GetHost()
        {
            return Host;
        }
    }
}