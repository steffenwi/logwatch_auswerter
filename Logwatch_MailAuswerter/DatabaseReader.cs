﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Logwatch_MailAuswerter
{
    class DatabaseReader
    {

        //Connection Aufbau
        private string connectionString;

        public DatabaseReader(string username, string password, string server, string databaseName)
        {
            SqlConnectionStringBuilder b = new SqlConnectionStringBuilder();
            b.UserID = username;
            b.Password = password;
            b.DataSource = server;
            b.InitialCatalog = databaseName;

            this.connectionString = b.ConnectionString;
        }
 

        //SpamAssassin Daten anhand des ausgewählten Datums aus der Datenbank auslesen

        public List<string>GetSpam(string date)
        {
            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string abfrage = "SELECT spam, clean FROM dbo.mail WHERE dateRange LIKE @datum";
                SqlCommand getSpam = new SqlCommand(abfrage, con);

                if(date == "Alle")
                {
                    getSpam.Parameters.AddWithValue("@datum", "%");
                }
                else
                {
                    getSpam.Parameters.AddWithValue("@datum", "%" + date);
                }

            
                SqlDataAdapter da = new SqlDataAdapter(getSpam);
                DataTable data = new DataTable();
                da.Fill(data);

                List<string> mail = new List<string>();
                foreach(DataRow row in data.Rows)
                {
                    mail.Add(row[0].ToString());
                }

                return mail;
            }
        }

        //SpamAssassin Daten anhand des ausgewählten Datums aus der Datenbank auslesen

        public List<string>GetClean(string date)
        {
            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string abfrage = "Select spam, clean FROM dbo.mail WHERE dateRange LIKE @datum";
                SqlCommand getClean = new SqlCommand(abfrage, con);

                if (date == "Alle")
                {
                    getClean.Parameters.AddWithValue("@datum", "%");
                }
                else
                {
                    getClean.Parameters.AddWithValue("@datum", "%" + date);
                }
                

                SqlDataAdapter da = new SqlDataAdapter(getClean);
                DataTable data = new DataTable();
                da.Fill(data);

                List<string> mail = new List<string>();
                foreach (DataRow row in data.Rows)
                {
                    mail.Add(row[1].ToString());
                }

                return mail;
            }
        }


        public List<string> GetDateRangeAlle()
        {
            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string abfrage = "Select dateRange FROM dbo.mail";
                SqlCommand getDateRange = new SqlCommand(abfrage, con);

                SqlDataAdapter da = new SqlDataAdapter(getDateRange);
                DataTable data = new DataTable();
                da.Fill(data);

                List<string> dateRange = new List<string>();
                foreach (DataRow row in data.Rows)
                {
                    dateRange.Add(row[0].ToString());
                }

                return dateRange;
            }
        }

        //Date Range Daten auslesen
        public List<string> GetDateRange(string date)
        {
            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string abfrage = "Select dateRange FROM dbo.mail WHERE dateRange LIKE @datum";
                SqlCommand getDateRange = new SqlCommand(abfrage, con);

                if (date == "Alle")
                {
                    getDateRange.Parameters.AddWithValue("@datum", "%");
                }
                else
                {
                    getDateRange.Parameters.AddWithValue("@datum", "%" + date);
                }

                SqlDataAdapter da = new SqlDataAdapter(getDateRange);
                DataTable data = new DataTable();
                da.Fill(data);

                List<string> dateRange = new List<string>();
                foreach (DataRow row in data.Rows)
                {
                    dateRange.Add(row[0].ToString());
                }

                return dateRange;
            }
        }

        //Erstellungsdatum der Logwatch E-Mail aus Datenbank auslesen 

        public List<string> GetInitDate()
        {
            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string abfrage = "Select dateInitiated FROM dbo.mail";
                SqlCommand getInitDate = new SqlCommand(abfrage, con);

                SqlDataAdapter da = new SqlDataAdapter(getInitDate);
                DataTable data = new DataTable();
                da.Fill(data);

                List<string> initDate = new List<string>();
                foreach(DataRow row in data.Rows)
                {
                    initDate.Add(row[0].ToString());
                }

                return initDate;
            }
        }

        //Sent Bytes Daten anhand des ausgewählten Datums auslesen

        public List<string> GetDeliveredBytes(string date)
        {
            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string abfrage = "Select bytesDelivered FROM dbo.mail WHERE dateRange LIKE @datum";
                SqlCommand getDeliveredBytes = new SqlCommand(abfrage, con);

                if (date == "Alle")
                {
                    getDeliveredBytes.Parameters.AddWithValue("@datum", "%");
                }
                else
                {
                    getDeliveredBytes.Parameters.AddWithValue("@datum", "%" + date);
                }

                SqlDataAdapter da = new SqlDataAdapter(getDeliveredBytes);
                DataTable data = new DataTable();
                da.Fill(data);

                List<string> deliveredBytes = new List<string>();
                foreach (DataRow row in data.Rows)
                {
                    deliveredBytes.Add(row[0].ToString());
                }

                return deliveredBytes;
            }
        }

        //Sent Bytes Daten anhand des ausgewählten Datums auslesen

        public List<string> GetSMTPBytes(string date)
        {
            using(SqlConnection con = new SqlConnection(this.connectionString))
            {
                string abfrage = "Select bytesSMTP FROM dbo.mail WHERE dateRange LIKE @datum";
                SqlCommand getSMTPBytes = new SqlCommand(abfrage, con);

                if (date == "Alle")
                {
                    getSMTPBytes.Parameters.AddWithValue("@datum", "%");
                }
                else
                {
                    getSMTPBytes.Parameters.AddWithValue("@datum", "%" + date);
                }

                SqlDataAdapter da = new SqlDataAdapter(getSMTPBytes);
                DataTable data = new DataTable();
                da.Fill(data);

                List<string> smtpBytes = new List<string>();
                foreach (DataRow row in data.Rows)
                {
                    smtpBytes.Add(row[0].ToString());
                }

                return smtpBytes;
            }
        }


        //Accepted Rejected Daten anhand des ausgewählten Datums auslesen
        public List<string> GetAccepted(string date)
        {
            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string abfrage = "Select accepted FROM dbo.mail WHERE dateRange LIKE @datum";
                SqlCommand getAccepted = new SqlCommand(abfrage, con);

                if (date == "Alle")
                {
                    getAccepted.Parameters.AddWithValue("@datum", "%");
                }
                else
                {
                    getAccepted.Parameters.AddWithValue("@datum", "%" + date);
                }

                SqlDataAdapter da = new SqlDataAdapter(getAccepted);
                DataTable data = new DataTable();
                da.Fill(data);

                List<string> accepted = new List<string>();
                foreach (DataRow row in data.Rows)
                {
                    accepted.Add(row[0].ToString());
                }

                return accepted;
            }
        }


        //Accepted Rejected Daten anhand des ausgewählten Datums auslesen
        public List<string> GetRejected(string date)
        {
            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string abfrage = "Select rejected FROM dbo.mail WHERE dateRange LIKE @datum";
                SqlCommand getRejected = new SqlCommand(abfrage, con);

                if (date == "Alle")
                {
                    getRejected.Parameters.AddWithValue("@datum", "%");
                }
                else
                {
                    getRejected.Parameters.AddWithValue("@datum", "%" + date);
                }

                SqlDataAdapter da = new SqlDataAdapter(getRejected);
                DataTable data = new DataTable();
                da.Fill(data);

                List<string> rejected = new List<string>();
                foreach(DataRow row in data.Rows)
                {
                    rejected.Add(row[0].ToString());
                }

                return rejected;
            } 
        }

        //Accepted Rejected Daten anhand des ausgewählten Datums auslesen
        public List<string> GetAcceptedRejected(string date)
        {
            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string abfrage = "Select acceptedRejectedTotal FROM dbo.mail WHERE dateRange LIKE @datum";
                SqlCommand getAcceptedRejected = new SqlCommand(abfrage, con);

                if (date == "Alle")
                {
                    getAcceptedRejected.Parameters.AddWithValue("@datum", "%");
                }
                else
                {
                    getAcceptedRejected.Parameters.AddWithValue("@datum", "%" + date);
                }

                SqlDataAdapter da = new SqlDataAdapter(getAcceptedRejected);
                DataTable data = new DataTable();
                da.Fill(data);

                List<string> acceptedRejected = new List<string>();
                foreach (DataRow row in data.Rows)
                {
                    acceptedRejected.Add(row[0].ToString());
                }

                return acceptedRejected;
            }
        }

        //5xx Daten anhand des ausgewählten Datums auslesen
        public List<string> GetRejectRelayDenied(string date)
        {
            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string abfrage = "Select rejectRelayDenied FROM dbo.mail WHERE dateRange LIKE @datum";
                SqlCommand getRejectRelayDenied = new SqlCommand(abfrage, con);

                if (date == "Alle")
                {
                    getRejectRelayDenied.Parameters.AddWithValue("@datum", "%");
                }
                else
                {
                    getRejectRelayDenied.Parameters.AddWithValue("@datum", "%" + date);
                }

                SqlDataAdapter da = new SqlDataAdapter(getRejectRelayDenied);
                DataTable data = new DataTable();
                da.Fill(data);

                List<string> rejectRelayDenied = new List<string>();
                foreach(DataRow row in data.Rows)
                {
                    rejectRelayDenied.Add(row[0].ToString());
                }

                return rejectRelayDenied;
            }
        }


        //5xx Daten anhand des ausgewählten Datums auslesen
        public List<string> GetRejectUnknownUser(string date)
        {
            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string abfrage = "Select rejectUnknownUser FROM dbo.mail WHERE dateRange LIKE @datum";
                SqlCommand getRejectUnknownUser = new SqlCommand(abfrage, con);

                if (date == "Alle")
                {
                    getRejectUnknownUser.Parameters.AddWithValue("@datum", "%");
                }
                else
                {
                    getRejectUnknownUser.Parameters.AddWithValue("@datum", "%" + date);
                }

                SqlDataAdapter da = new SqlDataAdapter(getRejectUnknownUser);
                DataTable data = new DataTable();
                da.Fill(data);

                List<string> rejectUnknownUser = new List<string>();
                foreach(DataRow row in data.Rows)
                {
                    rejectUnknownUser.Add(row[0].ToString());
                }

                return rejectUnknownUser;

            }
        }


        //5xx Daten anhand des ausgewählten Datums auslesen
        public List<string> GetRejectMessageSize(string date)
        {
            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string abfrage = "Select rejectMessageSize FROM dbo.mail WHERE dateRange LIKE @datum";
                SqlCommand getRejectMessageSize = new SqlCommand(abfrage, con);

                if (date == "Alle")
                {
                    getRejectMessageSize.Parameters.AddWithValue("@datum", "%");
                }
                else
                {
                    getRejectMessageSize.Parameters.AddWithValue("@datum", "%" + date);
                }

                SqlDataAdapter da = new SqlDataAdapter(getRejectMessageSize);
                DataTable data = new DataTable();
                da.Fill(data);

                List<string> rejectMessageSize = new List<string>();
                foreach (DataRow row in data.Rows)
                {
                    rejectMessageSize.Add(row[0].ToString());
                }

                return rejectMessageSize;
            }
        }


        //5xx Daten anhand des ausgewählten Datums auslesen
        public List<string> GetReject5xxTotal(string date)
        {
            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string abfrage = "Select reject5xxTotal FROM dbo.mail WHERE dateRange LIKE @datum";
                SqlCommand getReject5xxTotal = new SqlCommand(abfrage, con);

                if (date == "Alle")
                {
                    getReject5xxTotal.Parameters.AddWithValue("@datum", "%");
                }
                else
                {
                    getReject5xxTotal.Parameters.AddWithValue("@datum", "%" + date);
                }

                SqlDataAdapter da = new SqlDataAdapter(getReject5xxTotal);
                DataTable data = new DataTable();
                da.Fill(data);

                List<string> reject5xxTotal = new List<string>();
                foreach (DataRow row in data.Rows)
                {
                    reject5xxTotal.Add(row[0].ToString());
                }

                return reject5xxTotal;
            }
        }

        //4xx Daten anhand des ausgewählten Datums auslesen
        public List<string> GetRejectRecipientAddress(string date)
        {
            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string abfrage = "Select rejectRecipientAddress FROM dbo.mail WHERE dateRange LIKE @datum";
                SqlCommand getRejectRecipientAddress = new SqlCommand(abfrage, con);

                if (date == "Alle")
                {
                    getRejectRecipientAddress.Parameters.AddWithValue("@datum", "%");
                }
                else
                {
                    getRejectRecipientAddress.Parameters.AddWithValue("@datum", "%" + date);
                }

                SqlDataAdapter da = new SqlDataAdapter(getRejectRecipientAddress);
                DataTable data = new DataTable();
                da.Fill(data);

                List<string> recipientAddress = new List<string>();
                foreach (DataRow row in data.Rows)
                {
                    recipientAddress.Add(row[0].ToString());
                }

                return recipientAddress;
            }
        }

        //4xx Daten anhand des ausgewählten Datums auslesen
        public List<string> GetRejectUnknownClientHost(string date)
        {
            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                string abfrage = "Select rejectUnknownClientHost FROM dbo.mail WHERE dateRange LIKE @datum";
                SqlCommand getRejectUnknownClientHost = new SqlCommand(abfrage, con);

                if (date == "Alle")
                {
                    getRejectUnknownClientHost.Parameters.AddWithValue("@datum", "%");
                }
                else
                {
                    getRejectUnknownClientHost.Parameters.AddWithValue("@datum", "%" + date);
                }

                SqlDataAdapter da = new SqlDataAdapter(getRejectUnknownClientHost);
                DataTable data = new DataTable();
                da.Fill(data);

                List<string> unknownClientHost = new List<string>();
                foreach(DataRow row in data.Rows)
                {
                    unknownClientHost.Add(row[0].ToString());
                }

                return unknownClientHost;
            }
        }

        //4xx Daten anhand des ausgewählten Datums auslesen
        public List<string> GetReject4xxTotal(string date)
        {
            using(SqlConnection con = new SqlConnection(this.connectionString))
            {
                string abfrage = "Select reject4xxTotal FROM dbo.mail WHERE dateRange LIKE @datum";
                SqlCommand getReject4xxTotal = new SqlCommand(abfrage, con);

                if (date == "Alle")
                {
                    getReject4xxTotal.Parameters.AddWithValue("@datum", "%");
                }
                else
                {
                    getReject4xxTotal.Parameters.AddWithValue("@datum", "%"+ date);
                }

                SqlDataAdapter da = new SqlDataAdapter(getReject4xxTotal);
                DataTable data = new DataTable();
                da.Fill(data);

                List<string> reject4xxTotal = new List<string>();
                foreach(DataRow row in data.Rows)
                {
                    reject4xxTotal.Add(row[0].ToString());
                }

                return reject4xxTotal;
            }
        }


    }
}
