﻿namespace Logwatch_MailAuswerter
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_dock26 = new System.Windows.Forms.Button();
            this.ausgabe = new System.Windows.Forms.TextBox();
            this.checkDPKG = new System.Windows.Forms.CheckBox();
            this.checkHTTPD = new System.Windows.Forms.CheckBox();
            this.checkPostfix = new System.Windows.Forms.CheckBox();
            this.checkSSHD = new System.Windows.Forms.CheckBox();
            this.checkPam = new System.Windows.Forms.CheckBox();
            this.checkClam = new System.Windows.Forms.CheckBox();
            this.checkFail2Ban = new System.Windows.Forms.CheckBox();
            this.checkSpamAssassin = new System.Windows.Forms.CheckBox();
            this.checkDiskSpace = new System.Windows.Forms.CheckBox();
            this.checkDateRange = new System.Windows.Forms.CheckBox();
            this.checkDateInitiated = new System.Windows.Forms.CheckBox();
            this.checkHost = new System.Windows.Forms.CheckBox();
            this.btn_auswerten = new System.Windows.Forms.Button();
            this.btn_coastcom = new System.Windows.Forms.Button();
            this.btn_regio = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_dock26
            // 
            this.btn_dock26.Enabled = false;
            this.btn_dock26.Location = new System.Drawing.Point(12, 69);
            this.btn_dock26.Name = "btn_dock26";
            this.btn_dock26.Size = new System.Drawing.Size(126, 51);
            this.btn_dock26.TabIndex = 1;
            this.btn_dock26.Text = "ace.dock26.de";
            this.btn_dock26.UseVisualStyleBackColor = true;
            this.btn_dock26.Click += new System.EventHandler(this.btn_dock26_Click);
            // 
            // ausgabe
            // 
            this.ausgabe.Location = new System.Drawing.Point(289, 12);
            this.ausgabe.Multiline = true;
            this.ausgabe.Name = "ausgabe";
            this.ausgabe.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.ausgabe.Size = new System.Drawing.Size(479, 419);
            this.ausgabe.TabIndex = 3;
            // 
            // checkDPKG
            // 
            this.checkDPKG.AutoSize = true;
            this.checkDPKG.Location = new System.Drawing.Point(166, 81);
            this.checkDPKG.Name = "checkDPKG";
            this.checkDPKG.Size = new System.Drawing.Size(56, 17);
            this.checkDPKG.TabIndex = 4;
            this.checkDPKG.Text = "DPKG";
            this.checkDPKG.UseVisualStyleBackColor = true;
            // 
            // checkHTTPD
            // 
            this.checkHTTPD.AutoSize = true;
            this.checkHTTPD.Location = new System.Drawing.Point(166, 104);
            this.checkHTTPD.Name = "checkHTTPD";
            this.checkHTTPD.Size = new System.Drawing.Size(63, 17);
            this.checkHTTPD.TabIndex = 5;
            this.checkHTTPD.Text = "HTTPD";
            this.checkHTTPD.UseVisualStyleBackColor = true;
            // 
            // checkPostfix
            // 
            this.checkPostfix.AutoSize = true;
            this.checkPostfix.Location = new System.Drawing.Point(166, 127);
            this.checkPostfix.Name = "checkPostfix";
            this.checkPostfix.Size = new System.Drawing.Size(57, 17);
            this.checkPostfix.TabIndex = 6;
            this.checkPostfix.Text = "Postfix";
            this.checkPostfix.UseVisualStyleBackColor = true;
            // 
            // checkSSHD
            // 
            this.checkSSHD.AutoSize = true;
            this.checkSSHD.Location = new System.Drawing.Point(166, 150);
            this.checkSSHD.Name = "checkSSHD";
            this.checkSSHD.Size = new System.Drawing.Size(56, 17);
            this.checkSSHD.TabIndex = 7;
            this.checkSSHD.Text = "SSHD";
            this.checkSSHD.UseVisualStyleBackColor = true;
            // 
            // checkPam
            // 
            this.checkPam.AutoSize = true;
            this.checkPam.Location = new System.Drawing.Point(166, 173);
            this.checkPam.Name = "checkPam";
            this.checkPam.Size = new System.Drawing.Size(74, 17);
            this.checkPam.TabIndex = 8;
            this.checkPam.Text = "Pam_Unix";
            this.checkPam.UseVisualStyleBackColor = true;
            // 
            // checkClam
            // 
            this.checkClam.AutoSize = true;
            this.checkClam.Location = new System.Drawing.Point(166, 196);
            this.checkClam.Name = "checkClam";
            this.checkClam.Size = new System.Drawing.Size(49, 17);
            this.checkClam.TabIndex = 9;
            this.checkClam.Text = "Clam";
            this.checkClam.UseVisualStyleBackColor = true;
            // 
            // checkFail2Ban
            // 
            this.checkFail2Ban.AutoSize = true;
            this.checkFail2Ban.Location = new System.Drawing.Point(166, 219);
            this.checkFail2Ban.Name = "checkFail2Ban";
            this.checkFail2Ban.Size = new System.Drawing.Size(67, 17);
            this.checkFail2Ban.TabIndex = 10;
            this.checkFail2Ban.Text = "Fail2Ban";
            this.checkFail2Ban.UseVisualStyleBackColor = true;
            // 
            // checkSpamAssassin
            // 
            this.checkSpamAssassin.AutoSize = true;
            this.checkSpamAssassin.Location = new System.Drawing.Point(166, 242);
            this.checkSpamAssassin.Name = "checkSpamAssassin";
            this.checkSpamAssassin.Size = new System.Drawing.Size(94, 17);
            this.checkSpamAssassin.TabIndex = 11;
            this.checkSpamAssassin.Text = "SpamAssassin";
            this.checkSpamAssassin.UseVisualStyleBackColor = true;
            // 
            // checkDiskSpace
            // 
            this.checkDiskSpace.AutoSize = true;
            this.checkDiskSpace.Location = new System.Drawing.Point(166, 265);
            this.checkDiskSpace.Name = "checkDiskSpace";
            this.checkDiskSpace.Size = new System.Drawing.Size(78, 17);
            this.checkDiskSpace.TabIndex = 12;
            this.checkDiskSpace.Text = "DiskSpace";
            this.checkDiskSpace.UseVisualStyleBackColor = true;
            // 
            // checkDateRange
            // 
            this.checkDateRange.AutoSize = true;
            this.checkDateRange.Checked = true;
            this.checkDateRange.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkDateRange.Enabled = false;
            this.checkDateRange.Location = new System.Drawing.Point(166, 35);
            this.checkDateRange.Name = "checkDateRange";
            this.checkDateRange.Size = new System.Drawing.Size(84, 17);
            this.checkDateRange.TabIndex = 13;
            this.checkDateRange.Text = "Date Range";
            this.checkDateRange.UseVisualStyleBackColor = true;
            // 
            // checkDateInitiated
            // 
            this.checkDateInitiated.AutoSize = true;
            this.checkDateInitiated.Checked = true;
            this.checkDateInitiated.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkDateInitiated.Enabled = false;
            this.checkDateInitiated.Location = new System.Drawing.Point(166, 58);
            this.checkDateInitiated.Name = "checkDateInitiated";
            this.checkDateInitiated.Size = new System.Drawing.Size(89, 17);
            this.checkDateInitiated.TabIndex = 14;
            this.checkDateInitiated.Text = "Date Initiated";
            this.checkDateInitiated.UseVisualStyleBackColor = true;
            // 
            // checkHost
            // 
            this.checkHost.AutoSize = true;
            this.checkHost.Checked = true;
            this.checkHost.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkHost.Enabled = false;
            this.checkHost.Location = new System.Drawing.Point(166, 12);
            this.checkHost.Name = "checkHost";
            this.checkHost.Size = new System.Drawing.Size(48, 17);
            this.checkHost.TabIndex = 15;
            this.checkHost.Text = "Host";
            this.checkHost.UseVisualStyleBackColor = true;
            // 
            // btn_auswerten
            // 
            this.btn_auswerten.Location = new System.Drawing.Point(12, 380);
            this.btn_auswerten.Name = "btn_auswerten";
            this.btn_auswerten.Size = new System.Drawing.Size(126, 51);
            this.btn_auswerten.TabIndex = 16;
            this.btn_auswerten.Text = "Zur Diagramansicht";
            this.btn_auswerten.UseVisualStyleBackColor = true;
            this.btn_auswerten.Click += new System.EventHandler(this.btn_auswerten_Click);
            // 
            // btn_coastcom
            // 
            this.btn_coastcom.Location = new System.Drawing.Point(12, 12);
            this.btn_coastcom.Name = "btn_coastcom";
            this.btn_coastcom.Size = new System.Drawing.Size(126, 51);
            this.btn_coastcom.TabIndex = 0;
            this.btn_coastcom.Text = "Daten holen für mailgate.coastcom.de";
            this.btn_coastcom.UseVisualStyleBackColor = true;
            this.btn_coastcom.Click += new System.EventHandler(this.btn_coastcom_Click);
            // 
            // btn_regio
            // 
            this.btn_regio.Enabled = false;
            this.btn_regio.Location = new System.Drawing.Point(12, 126);
            this.btn_regio.Name = "btn_regio";
            this.btn_regio.Size = new System.Drawing.Size(126, 51);
            this.btn_regio.TabIndex = 2;
            this.btn_regio.Text = "sftp.regio-vertrieb.de";
            this.btn_regio.UseVisualStyleBackColor = true;
            this.btn_regio.Click += new System.EventHandler(this.btn_regio_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(790, 443);
            this.Controls.Add(this.btn_auswerten);
            this.Controls.Add(this.checkHost);
            this.Controls.Add(this.checkDateInitiated);
            this.Controls.Add(this.checkDateRange);
            this.Controls.Add(this.checkDiskSpace);
            this.Controls.Add(this.checkSpamAssassin);
            this.Controls.Add(this.checkFail2Ban);
            this.Controls.Add(this.checkClam);
            this.Controls.Add(this.checkPam);
            this.Controls.Add(this.checkSSHD);
            this.Controls.Add(this.checkPostfix);
            this.Controls.Add(this.checkHTTPD);
            this.Controls.Add(this.checkDPKG);
            this.Controls.Add(this.ausgabe);
            this.Controls.Add(this.btn_regio);
            this.Controls.Add(this.btn_dock26);
            this.Controls.Add(this.btn_coastcom);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btn_dock26;
        private System.Windows.Forms.TextBox ausgabe;
        private System.Windows.Forms.CheckBox checkDPKG;
        private System.Windows.Forms.CheckBox checkHTTPD;
        private System.Windows.Forms.CheckBox checkPostfix;
        private System.Windows.Forms.CheckBox checkSSHD;
        private System.Windows.Forms.CheckBox checkPam;
        private System.Windows.Forms.CheckBox checkClam;
        private System.Windows.Forms.CheckBox checkFail2Ban;
        private System.Windows.Forms.CheckBox checkSpamAssassin;
        private System.Windows.Forms.CheckBox checkDiskSpace;
        private System.Windows.Forms.CheckBox checkDateRange;
        private System.Windows.Forms.CheckBox checkDateInitiated;
        private System.Windows.Forms.CheckBox checkHost;
        private System.Windows.Forms.Button btn_auswerten;
        private System.Windows.Forms.Button btn_coastcom;
        private System.Windows.Forms.Button btn_regio;
    }
}

