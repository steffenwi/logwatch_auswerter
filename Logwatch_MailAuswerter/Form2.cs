﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using LiveCharts;
using LiveCharts.WinForms;
using System.Text.RegularExpressions;
using LiveCharts.Wpf;

namespace Logwatch_MailAuswerter
{
    public partial class Form2 : Form
    {

        DatabaseReader dbReader = new DatabaseReader("sa", "sa", @".\SQLEXPRESS", "Logwatch");

        public Form2()
        {
            InitializeComponent();


            //Dropdown Menü 1 befüllen
            comboBox1.Items.Add("Spam Assassin");
            comboBox1.Items.Add("Sent Bytes");
            comboBox1.Items.Add("Accepted/Rejected");
            comboBox1.Items.Add("Reject Reason 5xx");
            comboBox1.Items.Add("Reject Reason 4xx");
            comboBox1.DropDownStyle = ComboBoxStyle.DropDownList;
        }

        //Variablen deklarieren
        //SpamAssassin
        int clean = 0;
        int spam = 0;
        string dateRange = "";

        //Sent Bytes
        int deliveredBytes = 0;
        int SMTPBytes = 0;

        //Accepted/Rejected
        int accepted = 0;
        int rejected = 0;
        int acceptedRejected = 0;

        //Reject 5xx
        int relayDenied = 0;
        int unknownUser = 0;
        int messageSize = 0;
        int total5xx = 0;

        //Reject 4xx
        int recipientAddress = 0;
        int unknownClientHost = 0;
        int total4xx = 0;



        Func<ChartPoint, string> label = chartpoint => string.Format("{0} ({1:P})", chartpoint.Y, chartpoint.Participation);


        //Erstellung Chart SpamAssassin
        public void CreateSpamChart()
        {
            cartesianChart1.Series = new SeriesCollection
            {
                new ColumnSeries
                {
                    Title = "Spam",
                    Values = new ChartValues<int> {}
                }
            };

            cartesianChart1.Series.Add(new ColumnSeries
            {
                Title = "Clean",
                Values = new ChartValues<int> { }
            });

            foreach (var obj in dataSet1.DataTable1)
            {
                cartesianChart1.Series[0].Values.Add(obj.Spam);
                cartesianChart1.Series[1].Values.Add(obj.Clean);
            }
        }


        //SpamAssassin Daten holen
        public List<string> GetSpamData(string date)
        {
            List<string> spamList = dbReader.GetSpam(date);
            List<string> outPutListSpam = new List<string>();

            for (int i = 0; i < spamList.Count; i++)
            {
                if (spamList[i] != null && spamList[i] != "NULL" && spamList[i] != "" && spamList[i] != string.Empty)
                {
                    spam = Convert.ToInt32(spamList[i]);
                    outPutListSpam.Add(spam.ToString());
                }
            }

            return outPutListSpam;
        }

        //SpamAssassin Daten holen
        public List<string> GetCleanData(string date)
        {
            List<string> cleanList = dbReader.GetClean(date);
            List<string> outPutListClean = new List<string>();

            for (int i = 0; i < cleanList.Count; i++)
            {
                if (cleanList[i] != null && cleanList[i] != "NULL" && cleanList[i] != "" && cleanList[i] != string.Empty)
                {
                    clean = Convert.ToInt32(cleanList[i]);
                    outPutListClean.Add(clean.ToString());
                }
            }
            return outPutListClean;
        }

        //Date Range daten holen
        public List<string> GetDateRange(string date)
        {
            List<string> dateRangeList = dbReader.GetDateRange(date);
            List<string> outPutListDateRange = new List<string>();

            for (int i = 0; i < dateRangeList.Count; i++)
            {
                if (dateRangeList[i] != null && dateRangeList[i] != "")
                {
                    dateRange = dateRangeList[i];
                    outPutListDateRange.Add(dateRange);
                }
            }

            return outPutListDateRange;
        }

        public string GetDateRangeEinzeln(string date)
        {
            List<string> dateRangeList = dbReader.GetDateRange(date);

            for (int i = 0; i < dateRangeList.Count; i++)
            {
                dateRange = dateRangeList[i];
            }
            return dateRange;
        }

        //SpamAssassin Daten in DataSet laden
        public void FillSpamData()
        {
            List<string> spam = GetSpamData(comboBox2.SelectedItem.ToString());
            List<string> clean = GetCleanData(comboBox2.SelectedItem.ToString());
            List<string> dateRange = GetDateRange(comboBox2.SelectedItem.ToString());
            for (int i = 0; i < spam.Count; i++)
            {
                dataSet1.DataTable1.AddDataTable1Row(Convert.ToInt32(spam[i]), Convert.ToInt32(clean[i]), dateRange[i]);
            }
        }


        //Sent Bytes Daten holen
        public List<Int64> GetSMTPBytesData(string date)
        {
            List<string> SMTPBytesList = dbReader.GetSMTPBytes(date);
            List<Int64> outPutListSMTPBytes = new List<Int64>();

            for (int i = 0; i < SMTPBytesList.Count; i++)
            {
                if (SMTPBytesList[i] != null && SMTPBytesList[i] != "NULL" && SMTPBytesList[i] != "" && SMTPBytesList[i] != string.Empty)
                {
                    //SMTPBytes = Convert.ToInt32(SMTPBytesList[i]);
                    outPutListSMTPBytes.Add(Convert.ToInt64(SMTPBytesList[i]));
                }
            }
            return outPutListSMTPBytes;
        }


        //Sent Bytes Daten holen
        public List<Int64> GetDeliveredBytesData(string date)
        {
            List<string> deliveredBytesList = dbReader.GetDeliveredBytes(date);
            List<Int64> outPutListDeliveredBytes = new List<Int64>();

            for (int i = 0; i < deliveredBytesList.Count; i++)
            {
                if (deliveredBytesList[i] != null && deliveredBytesList[i] != "NULL" && deliveredBytesList[i] != "" && deliveredBytesList[i] != string.Empty)
                {
                    //deliveredBytes = Convert.ToInt32(deliveredBytesList[i]);
                    outPutListDeliveredBytes.Add(Convert.ToInt64(deliveredBytesList[i]));
                }
            }

            return outPutListDeliveredBytes;
        }



        //Sent Bytes Daten in DataSet laden
        public void FillBytesData()
        {
            List<Int64> SMTP = GetSMTPBytesData(comboBox2.SelectedItem.ToString());
            List<Int64> delivered = GetDeliveredBytesData(comboBox2.SelectedItem.ToString());
            
            for (int i = 0; i < SMTP.Count; i++)
            {
                dataSet1.DataTable2.AddDataTable2Row(Convert.ToInt64(SMTP[i]), Convert.ToInt64(delivered[i]));
            }
        }

        //Sent Bytes Chart erzeugen
        public void CreateBytesChart()
        {
            cartesianChart1.Series = new SeriesCollection
            {
                new ColumnSeries
                {
                    Title = "Bytes sent via SMTP",
                    Values = new ChartValues<Int64> { }
                }
            };

            cartesianChart1.Series.Add(new ColumnSeries
            {
                Title = "Bytes delivered",
                Values = new ChartValues<Int64> {  }
            });

            foreach (var obj in dataSet1.DataTable2)
            {
                cartesianChart1.Series[0].Values.Add(obj.SMTPBytes);
                cartesianChart1.Series[1].Values.Add(obj.DeliveredBytes);
            }
        }

        //Accepted Rejected Daten holen
        public List<string> GetAcceptedData(string date)
        {
            List<string> acceptedList = dbReader.GetAccepted(date);
            List<string> outPutListAccepted = new List<string>();

            for (int i = 0; i < acceptedList.Count; i++)
            {
                if (acceptedList[i] != null && acceptedList[i] != "NULL" && acceptedList[i] != "" && acceptedList[i] != string.Empty)
                {
                    accepted = Convert.ToInt32(acceptedList[i]);
                    outPutListAccepted.Add(accepted.ToString());
                }
            }

            return outPutListAccepted;
        }

        //Accepted Rejected Daten holen
        public List<string> GetRejectedData(string date)
        {
            List<string> rejectedList = dbReader.GetRejected(date);
            List<string> outPutListRejected = new List<string>();

            for (int i = 0; i < rejectedList.Count; i++)
            {
                if (rejectedList[i] != null && rejectedList[i] != "NULL" && rejectedList[i] != "" && rejectedList[i] != string.Empty)
                {
                    rejected = Convert.ToInt32(rejectedList[i]);
                    outPutListRejected.Add(rejected.ToString());
                }
            }
            return outPutListRejected;
        }

        //Accepted Rejected Daten holen
        public List<string> GetAcceptedRejectedData(string date)
        {
            List<string> acceptedRejectedList = dbReader.GetAcceptedRejected(date);
            List<string> outPutListAcceptedRejected = new List<string>();

            for (int i = 0; i < acceptedRejectedList.Count; i++)
            {
                if (acceptedRejectedList[i] != null && acceptedRejectedList[i] != "NULL" && acceptedRejectedList[i] != "" && acceptedRejectedList[i] != string.Empty)
                {
                    acceptedRejected = Convert.ToInt32(acceptedRejectedList[i]);
                    outPutListAcceptedRejected.Add(acceptedRejected.ToString());
                }
            }
            return outPutListAcceptedRejected;
        }

        //Accepted Rejected Daten in DataSet laden
        public void FillAcceptedRejectedData()
        {
            List<string> accepted = GetAcceptedData(comboBox2.SelectedItem.ToString());
            List<string> rejected = GetRejectedData(comboBox2.SelectedItem.ToString());
            List<string> acceptedRejected = GetAcceptedRejectedData(comboBox2.SelectedItem.ToString());

            for (int i = 0; i < accepted.Count; i++)
            {
                dataSet1.DataTable3.AddDataTable3Row(Convert.ToInt32(accepted[i]), Convert.ToInt32(rejected[i]), Convert.ToInt32(acceptedRejected[i]));
            }
        }

        //Accepted Rejected Daten in DataSet laden
        public void CreateAcceptedRejectedChart()
        {
            cartesianChart1.Series = new SeriesCollection
            {
                new ColumnSeries
                {
                    Title = "Total",
                    Values = new ChartValues<int> {}
                }
            };

            cartesianChart1.Series.Add(new ColumnSeries
            {
                Title = "Accepted",
                Values = new ChartValues<int> { }
            });

            cartesianChart1.Series.Add(new ColumnSeries
            {
                Title = "Rejected",
                Values = new ChartValues<int> { }
            });

            foreach (var obj in dataSet1.DataTable3)
            {
                cartesianChart1.Series[0].Values.Add(obj.AcceptedRejected);
                cartesianChart1.Series[1].Values.Add(obj.Accepted);
                cartesianChart1.Series[2].Values.Add(obj.Rejected);
            }
        }

        //5xx Daten holen
        public List<string> GetRelayDeniedData(string date)
        {
            List<string> relayDeniedList = dbReader.GetRejectRelayDenied(date);
            List<string> outPutListRelayDenied = new List<string>();

            for (int i = 0; i < relayDeniedList.Count; i++)
            {
                if (relayDeniedList[i] != null && relayDeniedList[i] != "NULL" && relayDeniedList[i] != "" && relayDeniedList[i] != string.Empty)
                {
                    relayDenied = Convert.ToInt32(relayDeniedList[i]);
                    outPutListRelayDenied.Add(relayDenied.ToString());
                }
            }

            return outPutListRelayDenied;
        }

        //5xx Daten holen
        public List<string> GetUnknownUserData(string date)
        {
            List<string> unknownUserList = dbReader.GetRejectUnknownUser(date);
            List<string> outPutListUnknownUser = new List<string>();

            for (int i = 0; i < unknownUserList.Count; i++)
            {
                if (unknownUserList[i] != null && unknownUserList[i] != "NULL" && unknownUserList[i] != "" && unknownUserList[i] != string.Empty)
                {
                    unknownUser = Convert.ToInt32(unknownUserList[i]);
                    outPutListUnknownUser.Add(unknownUser.ToString());
                }
            }

            return outPutListUnknownUser;
        }

        //5xx Daten holen
        public List<string> GetMessageSizeData(string date)
        {
            List<string> messageSizeList = dbReader.GetRejectMessageSize(date);
            List<string> outPutListMessageSize = new List<string>();

            for (int i = 0; i < messageSizeList.Count; i++)
            {
                if (messageSizeList[i] != null && messageSizeList[i] != "NULL" && messageSizeList[i] != "" && messageSizeList[i] != string.Empty)
                {
                    messageSize = Convert.ToInt32(messageSizeList[i]);
                    outPutListMessageSize.Add(messageSize.ToString());
                }
            }

            return outPutListMessageSize;
        }


        //5xx Daten holen
        public List<string> GetTotal5xxData(string date)
        {
            List<string> total5xxList = dbReader.GetReject5xxTotal(date);
            List<string> outPutList5xxTotal = new List<string>();

            for (int i = 0; i < total5xxList.Count; i++)
            {
                if (total5xxList[i] != null && total5xxList[i] != "NULL" && total5xxList[i] != "" && total5xxList[i] != string.Empty)
                {
                    total5xx = Convert.ToInt32(total5xxList[i]);
                    outPutList5xxTotal.Add(total5xx.ToString());
                }
            }

            return outPutList5xxTotal;
        }

        //5xx Daten in DataSet laden
        public void Fill5xxData()
        {
            List<string> relayDenied = GetRelayDeniedData(comboBox2.SelectedItem.ToString());
            List<string> unknownUser = GetUnknownUserData(comboBox2.SelectedItem.ToString());
            List<string> messageSize = GetMessageSizeData(comboBox2.SelectedItem.ToString());
            List<string> total5xx = GetTotal5xxData(comboBox2.SelectedItem.ToString());

            for (int i = 0; i < relayDenied.Count; i++)
            {
                dataSet1.DataTable4.AddDataTable4Row(Convert.ToInt32(relayDenied[i]), Convert.ToInt32(unknownUser[i]), Convert.ToInt32(messageSize[i]), Convert.ToInt32(total5xx[i]));
            }
        }

        //5xx Chart erstellen
        public void Create5xxChart()
        {
            cartesianChart1.Series = new SeriesCollection
            {
                new ColumnSeries
                {
                    Title = "Relay Denied",
                    Values = new ChartValues<int> {}
                }
            };

            cartesianChart1.Series.Add(new ColumnSeries
            {
                Title = "Unknown User",
                Values = new ChartValues<int> { }
            });

            cartesianChart1.Series.Add(new ColumnSeries
            {
                Title = "Message Size",
                Values = new ChartValues<int> { }
            });

            cartesianChart1.Series.Add(new ColumnSeries
            {
                Title = "Total",
                Values = new ChartValues<int> { }
            });

            foreach (var obj in dataSet1.DataTable4)
            {
                cartesianChart1.Series[0].Values.Add(obj.RelayDenied);
                cartesianChart1.Series[1].Values.Add(obj.UnknownUser);
                cartesianChart1.Series[2].Values.Add(obj.MessageSize);
                cartesianChart1.Series[3].Values.Add(obj.Total5xx);
            }
        }

        //4xx Daten holen
        public List<string> GetRecipientAddressData(string date)
        {
            List<string> recipientAddressList = dbReader.GetRejectRecipientAddress(date);
            List<string> outPutListRecipientAddress = new List<string>();

            for (int i = 0; i < recipientAddressList.Count; i++)
            {
                if (recipientAddressList[i] != null && recipientAddressList[i] != "NULL" && recipientAddressList[i] != "" && recipientAddressList[i] != string.Empty)
                {
                    recipientAddress = Convert.ToInt32(recipientAddressList[i]);
                    outPutListRecipientAddress.Add(recipientAddress.ToString());
                }
            }

            return outPutListRecipientAddress;
        }

        //4xx Daten holen
        public List<string> GetUnknownClientHostData(string date)
        {
            List<string> unknownClientHostList = dbReader.GetRejectUnknownClientHost(date);
            List<string> outPutListUnknownClientHost = new List<string>();

            for (int i = 0; i < unknownClientHostList.Count; i++)
            {
                if (unknownClientHostList[i] != null && unknownClientHostList[i] != "NULL" && unknownClientHostList[i] != "" && unknownClientHostList[i] != string.Empty)
                {
                    unknownClientHost = Convert.ToInt32(unknownClientHostList[i]);
                    outPutListUnknownClientHost.Add(unknownClientHost.ToString());
                }
            }

            return outPutListUnknownClientHost;
        }

        //4xx Daten holen
        public List<string> GetTotal4xxData(string date)
        {
            List<string> total4xxList = dbReader.GetReject4xxTotal(date);
            List<string> outPutListTotal4xx = new List<string>();

            for (int i = 0; i < total4xxList.Count; i++)
            {
                if (total4xxList[i] != null && total4xxList[i] != "NULL" && total4xxList[i] != "" && total4xxList[i] != string.Empty)
                {
                    total4xx = Convert.ToInt32(total4xxList[i]);
                    outPutListTotal4xx.Add(total4xx.ToString());
                }
            }

            return outPutListTotal4xx;
        }

        //4xx Daten in DataSet laden
        public void Fill4xxData()
        {
            List<string> recipientAddress = GetRecipientAddressData(comboBox2.SelectedItem.ToString());
            List<string> unknownClientHost = GetUnknownClientHostData(comboBox2.SelectedItem.ToString());
            List<string> total4xx = GetTotal4xxData(comboBox2.SelectedItem.ToString());

            for (int i = 0; i < recipientAddress.Count; i++)
            {
                dataSet1.DataTable5.AddDataTable5Row(Convert.ToInt32(recipientAddress[i]), Convert.ToInt32(unknownClientHost[i]), Convert.ToInt32(total4xx[i]));
            }
        }

        //4xx Chart erzeugen
        public void Create4xxChart()
        {
            cartesianChart1.Series = new SeriesCollection
            {
                new ColumnSeries
                {
                    Title = "Recipient Address Reject",
                    Values = new ChartValues<int> {}
                }
            };

            cartesianChart1.Series.Add(new ColumnSeries
            {
                Title = "Unknown Client Host Reject",
                Values = new ChartValues<int> { }
            });

            cartesianChart1.Series.Add(new ColumnSeries
            {
                Title = "Total",
                Values = new ChartValues<int> { }
            });



            foreach (var obj in dataSet1.DataTable5)
            {
                cartesianChart1.Series[0].Values.Add(obj.RecipientAddress);
                cartesianChart1.Series[1].Values.Add(obj.UnknownClientHost);
                cartesianChart1.Series[2].Values.Add(obj.Total4xx);
            }
        }


        //DropDown Menü 2 mit variablen Daten aus der Datenbank befüllen
        public void FillComboBox2()
        {
            List<string> comboBox2Items = dbReader.GetInitDate();
            List<string> comboBox2Items2 = dbReader.GetDateRangeAlle();

            for (int i = 0; i < comboBox2Items.Count; i++)
            {

                comboBox2.Items.Add(comboBox2Items2[i]);
                
                /*string[] itemArray = Regex.Split(comboBox2Items[i], @":");
                string item = itemArray[0];
                item = Regex.Replace(item, "00", "");
                item += comboBox2Items2[i];
                comboBox2.Items.Add(item);*/
            }
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            //Position der Chart-Legende festlegen und bearbeiten der 2. Dropdown Liste
            cartesianChart1.LegendLocation = LegendLocation.Right;
            comboBox2.Items.Add("Alle");
            FillComboBox2();
            comboBox2.SelectedIndex = 0;
            comboBox2.DropDownStyle = ComboBoxStyle.DropDownList;
        }


        //Ausführen der entsprechenden Methoden zur Erstellung des korrekten Diagramms
        private void button1_Click(object sender, EventArgs e)
        {
            //Spam Assassin
            if (comboBox1.SelectedIndex == 0)
            {
                    GetSpamData(comboBox2.SelectedItem.ToString());
                    GetCleanData(comboBox2.SelectedItem.ToString());
                    FillSpamData();
                    CreateSpamChart();
                if (comboBox2.SelectedItem.ToString() != "Alle")
                {
                    label3.Text = GetDateRangeEinzeln(comboBox2.SelectedItem.ToString());
                }
                else
                {
                    label3.Text = "";
                }
            }
            //Sent Bytes
            else if (comboBox1.SelectedIndex == 1)
            {
                    GetSMTPBytesData(comboBox2.SelectedItem.ToString());
                    GetDeliveredBytesData(comboBox2.SelectedItem.ToString());
                    FillBytesData();
                    CreateBytesChart();
                if (comboBox2.SelectedItem.ToString() != "Alle")
                {
                    label3.Text = GetDateRangeEinzeln(comboBox2.SelectedItem.ToString());
                }
                else
                {
                    label3.Text = "";
                }
            }
            //Accepted/Rejected
            else if (comboBox1.SelectedIndex == 2)
            {
                    GetAcceptedData(comboBox2.SelectedItem.ToString());
                    GetRejectedData(comboBox2.SelectedItem.ToString());
                    GetAcceptedRejectedData(comboBox2.SelectedItem.ToString());
                    FillAcceptedRejectedData();
                    CreateAcceptedRejectedChart();
                if (comboBox2.SelectedItem.ToString() != "Alle")
                {
                    label3.Text = GetDateRangeEinzeln(comboBox2.SelectedItem.ToString());
                }
                else
                {
                    label3.Text = "";
                }
            }
            //Reject 5xx
            else if (comboBox1.SelectedIndex == 3)
            {
                    GetRelayDeniedData(comboBox2.SelectedItem.ToString());
                    GetUnknownUserData(comboBox2.SelectedItem.ToString());
                    GetMessageSizeData(comboBox2.SelectedItem.ToString());
                    GetTotal5xxData(comboBox2.SelectedItem.ToString());
                    Fill5xxData();
                    Create5xxChart();
                if (comboBox2.SelectedItem.ToString() != "Alle")
                {
                    label3.Text = GetDateRangeEinzeln(comboBox2.SelectedItem.ToString());
                }
                else
                {
                    label3.Text = "";
                }
            }
            //Reject 4xx
            else if (comboBox1.SelectedIndex == 4)
            {
                    GetRecipientAddressData(comboBox2.SelectedItem.ToString());
                    GetUnknownClientHostData(comboBox2.SelectedItem.ToString());
                    GetTotal4xxData(comboBox2.SelectedItem.ToString());
                    Fill4xxData();
                    Create4xxChart();
                if (comboBox2.SelectedItem.ToString() != "Alle")
                {
                    label3.Text = GetDateRangeEinzeln(comboBox2.SelectedItem.ToString());
                }
                else
                {
                    label3.Text = "";
                }
            }
        }


        //Reset - Button zum zurücksetzen aller Charts
        private void button2_Click(object sender, EventArgs e)
        {
            cartesianChart1.Series.Clear();
            dataSet1.DataTable1.Clear();
            dataSet1.DataTable2.Clear();
            dataSet1.DataTable3.Clear();
            dataSet1.DataTable4.Clear();
            dataSet1.DataTable5.Clear();
            label3.Text = "";
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
