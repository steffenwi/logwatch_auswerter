﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Outlook;
using System.Text.RegularExpressions;


namespace Logwatch_MailAuswerter
{
    public partial class Form1 : Form
    {
        Microsoft.Office.Interop.Outlook.Application oApp = new Microsoft.Office.Interop.Outlook.Application();
        string zielFolderName;
        MAPIFolder QSourceFolder;
        DatabaseWriter dbWriter = new DatabaseWriter("sa", "sa", @".\SQLEXPRESS", "Logwatch");

        //Variablen Deklaration
        string host = "";
        string DPKG = "";
        string dateInitiated = "";
        string HTTPD = "";
        string postfix = "";
        string SSHD = "";
        string pam = "";
        string clam = "";
        string fail2ban = "";
        string spamAssassin = "";
        string diskSpace = "";
        string dateRange = "";

        //Spam Assassin
        string clear = "";
        string spam = "";

        //Sent Bytes
        string deliveredBytes = "";
        string SMTPBytes = "";

        //Accepted/Rejected
        string accepted = "";
        string rejected = "";
        string acceptedRejected = "";

        //Reject 5xx
        string relayDenied = "";
        string unknownUser = "";
        string messageSize = "";
        string total5xx = "";

        //Reject 4xx
        string recipientAddress = "";
        string unknownClientHost = "";
        string total4xx = "";

        public Form1()
        {
            //Festlegung des Outlook Kontos, in welchem die Logwatch Mails zu finden sind
            InitializeComponent();
            NameSpace oNS = (NameSpace)oApp.GetNamespace("MAPI");
            oNS.Logon(null, null, false, false);
            QSourceFolder = oNS.Folders["Öffentliche Ordner - steffen.witzel@dock26.de"];
            zielFolderName = "Infokonto";
        }
        
        string folderName = "Favoriten";
        MAPIFolder oSubfolder;
        MAPIFolder oZielFolder;
        Items mailItems = null;
        int counter = 1;

        //Auswahl, welcher Mailserver ausgewertet werden soll.
        private void btn_coastcom_Click(object sender, EventArgs e)
        {
            PrintLog("Logwatch for mailgate.coastcom.de");
        }

        private void btn_dock26_Click(object sender, EventArgs e)
        {
            PrintLog("Logwatch for ace.dock26.de");
        }

        private void btn_regio_Click(object sender, EventArgs e)
        { 
            PrintLog("Logwatch for sftp.regio-vertrieb.de");
        }

        void PrintLog(string logwatch)
        {
            try
            { 
                //E-Mail Folder Pfad Angabe
                oSubfolder = QSourceFolder.Folders[folderName];
                oZielFolder = oSubfolder.Folders[zielFolderName];
                mailItems = oZielFolder.Items;
                
                //Jede Mail im Zielfolder betrachten
                foreach (MailItem mailItem in oZielFolder.Items)
                {
                    
                    //Wenn kein Betreff:
                    if (mailItem.Subject == string.Empty || mailItem.Subject == null)
                    {
                       // NOTHING
                    }
                    //Wenn Betreff den entsprechenden Mailserver enthält:
                    else if (mailItem.Subject.Contains(logwatch))
                    {
                        //Ausgabe im Programm
                        ausgabe.Text = "Mail Nr. #" + counter + " wird verarbeitet";
                        string[] parts = mailItem.Body.Split('\n');

                        StatMail m = new StatMail(mailItem);


                        //Auslesen der E-Mails im Ziel-Folder
                        //Ziel Mails zerlegen in Einzelteile um sie besser abspeichern zu können
                        //Abspeichern der interessanten Informationen in Variablen.
                        if (checkHost.Checked == true)
                        {
                            host = m.GetHost();
                        }

                        if (checkDateInitiated.Checked == true)
                        {
                            dateInitiated = m.GetDateInitiated();

                            string[] dateInitiatedArray = Regex.Split(dateInitiated, @"Processing Initiated:");
                            dateInitiated = dateInitiatedArray[1];
                        }

                        if (checkDPKG.Checked == true)
                        {
                            DPKG = m.GetDPKG();
                        }

                        if (checkHTTPD.Checked == true)
                        {
                            HTTPD = m.GetHTTPD();
                        }

                        if (checkPostfix.Checked == true)
                        {

                            postfix = m.GetPostfix();
                            List<string> postfixLines = postfix.Split('\n').ToList().Select(x => x.Trim()).ToList();
                            int i = 0;

                            while (i < postfixLines.Count)
                            {
                                string line = postfixLines[i];


                                if (line == string.Empty)
                                {
                                    
                                }

                                else if (line.Contains("Accepted"))
                                {
                                    string[] acceptedArray = Regex.Split(line, @"Accepted");
                                    accepted = acceptedArray[0];
                                    accepted = Regex.Replace(accepted, " ", "");
                                }
                                else if (line.Contains("Rejected"))
                                {
                                    string[] rejectedArray = Regex.Split(line, @"Rejected");
                                    rejected = rejectedArray[0];
                                    rejected = Regex.Replace(rejected, " ", "");

                                    int totalLines = i + 2;
                                    line = postfixLines[totalLines];
                                    if (line.Contains("Total"))
                                    {
                                        string[] acceptedRejectedArray = Regex.Split(line, @"Total");
                                        acceptedRejected = acceptedRejectedArray[0];
                                        acceptedRejected = Regex.Replace(acceptedRejected, " ", "");
                                    }
                                }
                                else if (line.Contains("Bytes sent via SMTP"))
                                {
                                    string[] SMTPArray = Regex.Split(line, @"Bytes sent via SMTP");
                                    SMTPBytes = SMTPArray[1];
                                    SMTPBytes = Regex.Replace(SMTPBytes, ",", "");
                                    SMTPBytes = Regex.Replace(SMTPBytes, " ", "");
                                }
                                else if (line.Contains("Bytes delivered"))
                                {
                                    string[] deliveredBytesArray = Regex.Split(line, @"Bytes delivered");
                                    deliveredBytes = deliveredBytesArray[1];
                                    deliveredBytes = Regex.Replace(deliveredBytes, ",", "");
                                    deliveredBytes = Regex.Replace(deliveredBytes, " ", "");
                                }

                                else if (line.Contains("5xx Reject relay denied"))
                                {
                                    string[] relayDeniedArray = Regex.Split(line, @"5xx Reject relay denied");
                                    relayDenied = relayDeniedArray[0];
                                    relayDenied = Regex.Replace(relayDenied, " ", "");
                                }
                                else if (line.Contains("5xx Reject unknown user"))
                                {
                                    string[] unknownUserArray = Regex.Split(line, @"5xx Reject unknown user");
                                    unknownUser = unknownUserArray[0];
                                    unknownUser = Regex.Replace(unknownUser, " ", "");
                                }
                                else if (line.Contains("5xx Reject message size"))
                                {
                                    string[] messageSizeArray = Regex.Split(line, @"5xx Reject message size");
                                    messageSize = messageSizeArray[0];
                                    messageSize = Regex.Replace(messageSize, " ", "");
                                }
                                else if (line.Contains("Total 5xx Rejects"))
                                {
                                    string[] total5xxArray = Regex.Split(line, @"Total 5xx Rejects");
                                    total5xx = total5xxArray[0];
                                    total5xx = Regex.Replace(total5xx, " ", "");
                                }
                                else if (line.Contains("4xx Reject recipient address"))
                                {
                                    string[] recipientAddressArray = Regex.Split(line, @"4xx Reject recipient address");
                                    recipientAddress = recipientAddressArray[0];
                                    recipientAddress = Regex.Replace(recipientAddress, " ", "");
                                }
                                else if (line.Contains("4xx Reject unknown client host"))
                                {
                                    string[] unknownClientHostArray = Regex.Split(line, @"4xx Reject unknown client host");
                                    unknownClientHost = unknownClientHostArray[0];
                                    unknownClientHost = Regex.Replace(unknownClientHost, " ", "");
                                }
                                else if(line.Contains("Total 4xx Rejects"))
                                {
                                    string[] total4xxArray = Regex.Split(line, @"Total 4xx Rejects");
                                    total4xx = total4xxArray[0];
                                    total4xx = Regex.Replace(total4xx, " ", "");
                                }
                                
                                i++;
                            }

                        }

                        if (checkSSHD.Checked == true)
                        {
                            SSHD = m.GetSSHD();
                        }
                        
                        if (checkPam.Checked == true)
                        {
                            pam = m.GetPam_Unix();
                        }

                        if (checkClam.Checked == true)
                        {
                            clam = m.GetClam();
                        }

                        if (checkFail2Ban.Checked == true)
                        {
                            fail2ban = m.GetFail2Ban();
                        }

                        if (checkSpamAssassin.Checked == true)
                        {
                            spamAssassin = m.GetSpamAssassin();
                            if (spamAssassin.Contains("Total Spam:"))
                            {
                                string[] spamArray = Regex.Split(spamAssassin, @"Total Spam:");
                                spam = spamArray[1];
                                spamArray = Regex.Split(spam, @"\(");
                                spam = spamArray[0];
                                spam = Regex.Replace(spam, " ", "");

                                string[] clearArray = Regex.Split(spamAssassin, @"Total Clean:");
                                clear = clearArray[1];
                                clearArray = Regex.Split(clear, @"\(");
                                clear = clearArray[0];
                                clear = Regex.Replace(clear, " ", "");
                            }
                            
                        }

                        if (checkDiskSpace.Checked == true)
                        {
                            diskSpace = m.GetDiskSpace();
                        }

                        if (checkDateRange.Checked == true) 
                        {
                            dateRange = m.GetDateRange();
                        }

                        //Bei jeder fertigen Mail fragen, ob sie in die Datenbank gespeichert werden soll
                        DialogResult dialogResult = MessageBox.Show("Möchten Sie die Mail Nummer " + counter + " in der Datenbank speichern?", "In Datenbank  speichern?", MessageBoxButtons.YesNo);
                        //Wenn ja, alle Variablen in die Datenbank schreiben
                        if (dialogResult == DialogResult.Yes)
                        {
                            dbWriter.WriteEmail(host, DPKG, HTTPD, postfix, SSHD, pam, clam, fail2ban, spamAssassin, diskSpace, dateRange, dateInitiated, spam, clear, SMTPBytes, deliveredBytes, accepted, rejected, acceptedRejected, relayDenied, unknownUser, messageSize, total5xx, recipientAddress, unknownClientHost, total4xx);
                            ausgabe.Text = "";
                            recipientAddress = "0";
                            messageSize = "0";
                        }
                        //Wenn nein, Datensatz überspringen und mit dem nächsten weiter machen
                        else
                        {
                            ausgabe.Text = "";
                            recipientAddress = "0";
                            messageSize = "0";
                        }

                        counter++;
                    }
                    else
                    {
                        //Nothing to see here!
                    }  
                }
            }

            finally
            {
                //Counter zählt mit, wie viele Mails schon verarbeitet wurden
                counter = counter - 1;
            }
        }

        //Zur Auswertungs-Seite
        private void btn_auswerten_Click(object sender, EventArgs e)
        {
            Form2 form2 = new Form2();
            form2.Show();
            Hide();
        }
    }
}
