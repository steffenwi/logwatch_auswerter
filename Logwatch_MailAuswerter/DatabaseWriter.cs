﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace Logwatch_MailAuswerter
{
    class DatabaseWriter
    {

        //Connection Aufbau
        private string connectionString;

        public DatabaseWriter(string username, string password, string server, string databaseName)
        {
            SqlConnectionStringBuilder b = new SqlConnectionStringBuilder();
            b.UserID = username;
            b.Password = password;
            b.DataSource = server;
            b.InitialCatalog = databaseName;

            this.connectionString = b.ConnectionString;
        }

        //SQL-Statement zum Inserten in die Datenbank

        public void WriteEmail(string host, string DPKG, string HTTPD, string postfix, string SSHD, string pam, string clam, string fail2ban, string spamAssassin, 
            string diskSpace, string dateRange, string dateInitiated, string spam, string clear, string SMTPBytes, string deliveredBytes, string accepted, string rejected, 
            string acceptedRejected, string relayDenied, string unknownUser, string messageSize, string total5xx, string recipientAddress, string unknownClientHost, string total4xx)
        {
            using (SqlConnection con = new SqlConnection(this.connectionString))
            {
                SqlCommand writeEmail = new SqlCommand("Insert into dbo.mail (host, dateInitiated, DPKG, HTTPD, postfix, SSHD, pam_unix, clam, fail2ban, " +
                    "spamAssassin, diskSpace, dateRange, spam, clean, bytesSMTP, bytesDelivered, accepted, rejected, acceptedRejectedTotal, rejectRelayDenied, rejectUnknownUser, " +
                    "rejectMessageSize, reject5xxTotal, rejectRecipientAddress, rejectUnknownClientHost, reject4xxTotal) VALUES (@host, @dateInitiated, @DPKG, @HTTPD, @postfix, " +
                    "@SSHD, @pam_unix, @clam, @fail2ban, @spamAssassin, @diskSpace, @dateRange, @spam, @clear, @bytesSMTP, @bytesDelivered, @accepted, @rejected, @acceptedRejected, " +
                    "@relayDenied, @unknownUser, @messageSize, @total5xx, @recipientAddress, @unknownClientHost, @total4xx)", con);

                writeEmail.Parameters.AddWithValue("@host", host);
                writeEmail.Parameters.AddWithValue("@dateInitiated", dateInitiated);
                writeEmail.Parameters.AddWithValue("@DPKG", DPKG);
                writeEmail.Parameters.AddWithValue("@HTTPD", HTTPD);
                writeEmail.Parameters.AddWithValue("@postfix", postfix);
                writeEmail.Parameters.AddWithValue("@SSHD", SSHD);
                writeEmail.Parameters.AddWithValue("@pam_unix", pam);
                writeEmail.Parameters.AddWithValue("@clam", clam);
                writeEmail.Parameters.AddWithValue("@fail2ban", fail2ban);
                writeEmail.Parameters.AddWithValue("@spamAssassin", spamAssassin);
                writeEmail.Parameters.AddWithValue("@spam", spam);
                writeEmail.Parameters.AddWithValue("@clear", clear);
                writeEmail.Parameters.AddWithValue("@diskSpace", diskSpace);
                writeEmail.Parameters.AddWithValue("@dateRange", dateRange);
                writeEmail.Parameters.AddWithValue("@bytesSMTP", SMTPBytes);
                writeEmail.Parameters.AddWithValue("@bytesDelivered", deliveredBytes);
                writeEmail.Parameters.AddWithValue("@accepted", accepted);
                writeEmail.Parameters.AddWithValue("@rejected", rejected);
                writeEmail.Parameters.AddWithValue("@acceptedRejected", acceptedRejected);
                writeEmail.Parameters.AddWithValue("@relayDenied", relayDenied);
                writeEmail.Parameters.AddWithValue("@unknownUser", unknownUser);
                writeEmail.Parameters.AddWithValue("@messageSize", messageSize);
                writeEmail.Parameters.AddWithValue("@total5xx", total5xx);
                writeEmail.Parameters.AddWithValue("@recipientAddress", recipientAddress);
                writeEmail.Parameters.AddWithValue("@unknownClientHost", unknownClientHost);
                writeEmail.Parameters.AddWithValue("@total4xx", total4xx);

                con.Open();
                writeEmail.ExecuteNonQuery();
            }
        }
    }
}
